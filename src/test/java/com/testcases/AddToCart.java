package com.testcases;

import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.home.Reusables.ResuablesActions;
import com.library.UtilityScreen;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(ListernersDemo.TestngListerner.class)
public class AddToCart extends ConfigClass{
	
@Test
public void VerifyCart()
{
	try{     
		  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		 
		  test = extent.startTest(this.getClass().getSimpleName());
		  System.out.println(driver.getTitle());
		  assertTrue(driver.getTitle().contains("Buy Sports T-Shirts, Tracks, Running Shoes and Accessories Online - in.puma.com"));
		  test.log(LogStatus.PASS, "Verification of Page title is successful");
		  test.log(LogStatus.INFO, "Verification of PUMA e commerce page"  + this.getClass().getSimpleName());
		  
		  test.setDescription("Verification of PUMA e commerce page ");
		  ResuablesActions.moveToelement("NavigatetoMen","Running");

		  
		  String itemName=ResuablesActions.GetAttribute("productname", "proname");
		  System.out.println(itemName);
		 driver=ResuablesActions.ClickandSwitchToWindow(driver,"clickseconditem");

		  System.out.println(itemName);
		  System.out.println(driver.getTitle());
		  
		  ResuablesActions.assertThat(driver.getTitle(),itemName);
		 //System.out.println(ResuablesActions.getelement("clicksizetable").isDisplayed());
		  ResuablesActions.JavaScriptClickOnElement("clicksizetable");

		  ResuablesActions.SleepTime(3000);
		 
		  ResuablesActions.selectsize("sizeselect");
		  
		  
		  UtilityScreen.CaptureScreenshot(driver);
		  ResuablesActions.SleepTime(2000);
		ResuablesActions.assertThat(ResuablesActions.getelement("prodname").getText(),itemName);
		  
		  
	}
	catch(AssertionError  A)
     {
		 test.log(LogStatus.FAIL, "Test Case  Verification of Page title has failed");
		 A.printStackTrace();
	 }
	catch(Exception e)
	{ test.log(LogStatus.FAIL, "Test Case  Verification of Page title has failed");
	 e.printStackTrace();
		
	}
			
}


}
