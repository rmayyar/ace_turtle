package com.testcases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.library.ExcelLibrary;
import com.library.LoggerInfo;
import com.library.MyException;
import com.library.SendEmail;
import com.library.UtilityScreen;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class ConfigClass extends MyException
{
	/*Global Variables*/
	public  static WebDriver driver;
	/*WebDriver Config*/
	
	public static ExcelLibrary lib = new ExcelLibrary();
	/*Excel Library to load*/
	
	public static Properties prop = new Properties();
	/*Load Properties File*/
	
	
	
	
	private static String FILE_PATH = "C:/Ace_Turtle/Reports/ACE_Turtle_Suite_Report";
	private static String FILE_EXTENSION = ".html";
	static DateFormat df = new SimpleDateFormat("yyyyMMdd"); // add S if you need milliseconds
	static SimpleDateFormat sdfDate = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss.SSS");// dd/MM/yyyy
	static Date now = new Date(System.currentTimeMillis());
   static  String strDate = sdfDate.format(now);
	static String filename = FILE_PATH + strDate.replace(":","_").replace(".","_")+ "." + FILE_EXTENSION;
	/*Extent Report Path which includes FileName.html*/
	
	public static ExtentReports extent = new ExtentReports(filename, true);
	/*Extent Reports Configuration*/
	
	public static ExtentTest test;
	/*Extent Report method*/
	
	public static Logger logger = LoggerInfo.getLogger(UtilityScreen.class.getClass().getName());
	/*Logger File Configuration*/
	
	public static String extFile = "C:/Ace_Turtle/extentreports-java-2.41.2/extent-config.xml";
	/*Extent Report configuration file*/
	
	public static String SourceFile = "C:/Ace_Turtle/LocatorProperties/Object.properties";
	/*Object Properties file Configuration*/
	
	public static String chromeDriverLaunch = "C:/Ace_Turtle/ExternalLib/chromedriver.exe";
	/*Chrome Driver*/
	
	public static String iEdriverLaunch = "C:/Ace_Turtle/ExternalLib/IEDriverServer.exe";
	public static String gekodriver="C:/Ace_Turtle/ExternalLib/geckodriver.exe";
	/*IE Driver*/
	
	//public static CBDFields CreateCBD= new CBDFields();
	
							@BeforeSuite
							
							public void ExtentReports()
							{
								/*Configuration of Extent Reports*/
								  
								extent.loadConfig(new File(extFile));
								
								
							}
							
							
														@AfterSuite
														public void FlushExtentReports()
														{
															/*Flush is the method used to generate reports*/
															
															extent.flush();
															
															logger.info("Extent Report Flushed and Report is available now");
															
															/*Sending Email*/
															
													    	SendEmail.sendMailWithAttachment(filename);
													    	
													    	
													    	logger.info("Email Sent with attached execution report");
													    	
														}
							
							@BeforeMethod
							public void LaunchApplication() throws IOException
							{
								String browser, appurl;
								
							//browser=lib.getExcelData("Config", 1, 2);
							browser="chrome";
							
							appurl="https://in.puma.com";
							//appurl=lib.getExcelData("Config", 1, 0);
								
							//	logger.info("Test Execution started for " + this.getClass().getSimpleName());
								
								File src = new File(SourceFile);
								
								FileInputStream fis = null;
								
								try {fis = new FileInputStream(src);} catch (FileNotFoundException e) {e.printStackTrace();}
								
								try {prop.load(fis);} catch (IOException e) {e.printStackTrace();}
								
										if(browser.equalsIgnoreCase("firefox"))
										{
										
											//System.setProperty("webdriver.gecko.driver", gekodriver);
												
												
												
												FirefoxProfile prof = new FirefoxProfile(); 
												prof.setPreference("browser.startup.homepage_override.mstone", "ignore"); 
												prof.setPreference("startup.homepage_welcome_url.additional", "about:blank"); 
												driver = new FirefoxDriver(prof);
												
										           
											//	driver= new FirefoxDriver();
												prof.setPreference("browser.download.folderList", 2);
												prof.setPreference("browser.helperapps.neverAsk.saveToDisk","jpeg");
												prof.setPreference("browser.download.dir", "C:/Ace_Turtle/Pictures");
										}
												if(browser.equalsIgnoreCase("chrome"))
												{
														System.setProperty("webdriver.chrome.driver", chromeDriverLaunch);
														/*ChromeOptions o = new ChromeOptions();
														o.addArguments("disable-extensions");
														o.addArguments("--start-maximized");
														 driver = new ChromeDriver(o);*/
														driver = new ChromeDriver();
												}
													if(browser.equalsIgnoreCase("ie"))
													{	
														System.setProperty("webdriver.ie.driver", iEdriverLaunch);
														
														DesiredCapabilities dc = new DesiredCapabilities();
						                                dc.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING,false);
						                                dc.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, false);
						                                dc.setCapability(InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, true);
						                                dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true); 
						                                dc.setCapability("InternetExplorerDriver.ignoreProtectedModeSettings", true);
						                                dc.setCapability("InternetExplorerDriver..ie.forceCreateProcessApi", true);
						                                dc.setCapability("InternetExplorerDriver.ie.ensureCleanSession", true);
						                                dc.setCapability("InternetExplorerDriver.ie.setProxyByServer", true);
						                                dc.setJavascriptEnabled(true);
						                               
						                                driver = new InternetExplorerDriver(dc);
													}
									driver.manage().window().maximize();
									
									driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
									
									driver.get(appurl);
							}
							
														@AfterMethod
														public void LogoutApplication()
														{
															driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
															
															extent.endTest(test);
															
															try {Thread.sleep(2000);} catch (InterruptedException e) {e.printStackTrace();}
															
															driver.close();
															
															logger.info("Test Case Execution Completed for " + this.getClass().getSimpleName());
															driver.quit();
															
														}


														public NumberFormat getFormat(Locale locale) {
															// TODO Auto-generated method stub
															return null;
														}
}
