package com.home.Reusables;

import static org.testng.Assert.assertTrue;

import java.awt.Desktop.Action;
import java.awt.Robot;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.validator.routines.EmailValidator;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import com.relevantcodes.extentreports.LogStatus;
import com.testcases.ConfigClass;
import com.google.common.base.Function;
import org.openqa.selenium.support.ui.FluentWait;
import com.library.*;
 

public class ResuablesActions extends ConfigClass
{
	
	
	
	
								public static void SleepTime(int time)
								{
									/*This method is to Sleep for specific time{Exceplite Wait} which is passed as a parameter*/
									
									try {Thread.sleep(time);} catch (InterruptedException e) {e.printStackTrace();logger.info("TimedOut Exception Occured");}
								}
	
								
								
	
	public static void SelectByVisibleText(String eleLocator, String dropDownOptionValue)
	{
		try
		{
		/*String "eleLocator"  is webElement to Select DropDown Value*/
		
		//WebDriverWait wait = new WebDriverWait(driver, 15);
		
		//WebElement ele = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty(eleLocator))));
			WebElement ele=ResuablesActions.getelement(eleLocator);
		Select dropDownOption = new Select(ele);
		
		/*Parameters Element to Select from Drop down*/
		
		List<WebElement> allOptions=dropDownOption.getOptions();
		for(WebElement e:allOptions)
		{
			test.log(LogStatus.INFO," Department Drop down Options are\t "+e.getText());
		}
		
		dropDownOption.selectByVisibleText(dropDownOptionValue);
		
		test.log(LogStatus.INFO, "Selected value from dropdown is "+dropDownOptionValue);
		
		}catch(org.openqa.selenium.TimeoutException ex)
		{
			logger.info("Element Not Found Exception " + ex.getMessage());
			
			test.log(LogStatus.FAIL, "Element Not Found Exception " + ex.getMessage());
			
			throw(ex);
		}
	}
	
	
								public static  void SetTextToWebElement(String eleLocator, String val,boolean ifmandatory)
								{
									try
									{
									WebDriverWait wait = new WebDriverWait(driver, 20);
									
									
									WebElement ele =ResuablesActions.getelement(eleLocator);
									
									ele.clear();
									
									ele.sendKeys(val);
									test.log(LogStatus.INFO, "field\t"+eleLocator+"\tfilled with the value\t"+val);
									ResuablesActions.SleepTime(500);
									ele.sendKeys(Keys.TAB);
									}catch(org.openqa.selenium.TimeoutException ex)
									{
										logger.info("Element Not Found Exception " + ex.getMessage());
										
										test.log(LogStatus.FAIL, "Element Not Found Exception " + ex.getMessage());
										
										throw(ex);
									}
								}
								
									
								
	
	
			
	
								
								
								
								
								/*Get element Method , Waits for the Element , Get the elemetn and Scroll to the Perticular element*/
								public static WebElement getelement(String ele)
								{  
									WebElement elem=null;
									try
									{

									//ResuablesActions.waitforElement(ele);

									 
										elem=driver.findElement(By.xpath(prop.getProperty(ele)));
								     
									ResuablesActions.scolltoele(elem);
									
										test.log(LogStatus.INFO, ""+"\t Element "+ele+"\t"+" ,found");
								    }	
									catch(Exception e)
									{
										test.log(LogStatus.INFO, ""+"\t While getting the element\t"+ele+"\t"+" , Element Not found");
									}
									return elem;
								}
								
								
							
								public static void scolltoele(WebElement elem) throws NoSuchElementException
								{
									((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elem);
									ResuablesActions.SleepTime(500);
								}
								/*Explicit wait , waits till the visibility of the element is located*/
                public static void wait(String ele)
                {
                	WebDriverWait wait = new WebDriverWait(driver, 10);
                	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty(ele))));
                }
                /*Is clickable method checks whether the Element is clickable*/
                public static boolean isClickable(WebElement webe)      
                {
                try
                {
                   WebDriverWait wait = new WebDriverWait(driver, 5);
                   wait.until(ExpectedConditions.elementToBeClickable(webe));
                   return true;
                }
                catch (Exception e)
                {
                  return false;
                }
                }
                /*Fluent wait  */
                public static void waitforElement(final String elem)
                {
                	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(20, TimeUnit.SECONDS)
                            		   .pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class).ignoring(ElementNotVisibleException.class);
                	try {
             				wait.until(new com.google.common.base.Function<WebDriver, WebElement>()
             				{
             					public WebElement apply(WebDriver driver)
             					{
                            	      
                            	        	    WebDriverWait wait = new WebDriverWait(driver, 5);
                            	        	    System.out.println(prop.getProperty(elem));
                            	        	  System.out.println(driver.findElement(By.xpath(prop.getProperty(elem))).isDisplayed());
                            	        	    wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(prop.getProperty(elem)))));
                            	        	    
                            	           
                            	           
                            	        	    return driver.findElement(By.xpath(prop.getProperty(elem)));
                            	    
             					}
             				});
                		}
		             	catch (TimeoutException ex)
		             	{    
		             		
		             	 	test.log(LogStatus.INFO, "Invalid locator or locator not found:," + elem+" ,"+elem+" locator does not exists in the screen");
		             	try
		             		{
		             			throw new MyException();
		             		}
							catch(MyException my)
							{
								
				             		test.log(LogStatus.INFO, "Invalid locator or locator not found:," + elem+" ,"+elem+" locator does not exists in the screen");
							}
		             	}
                	
              }
                	
    
  
            	public static WebElement Containslink(String Text,boolean elimentnotclickable, boolean ifmandatory)
				{  
				//ResuablesActions.waitforElement(Text,true,elimentnotclickable, ifmandatory);
            	 //WebDriverWait wait = new WebDriverWait(driver, 60);
 	        	   // wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(Text))));
					Text="(.//*[contains(text(),'"+Text+"')])";
					System.out.println(Text);
					WebElement elem=null;
					try{
						
					 elem=driver.findElement(By.xpath(Text));
					 test.log(LogStatus.INFO, ""+"\t Element "+Text+"\t"+" ,found");
					ResuablesActions.scolltoele(elem);
					
				    }	
					catch(Exception e)
					{
						test.log(LogStatus.INFO, ""+Text+"\t While getting the element , Element Not found");
					}
					return elem;
				}
 	
 	
            	public static void clicktonele(String ele)
            	{
            		try {
            		
            		
            		ResuablesActions.getelement(ele).click();
            		
            		}
            		catch(Exception e)
            		{
            			test.log(LogStatus.FAIL, "Error while clicking on element " +ele);
            			e.printStackTrace();
            		}
            	}
            	
            	public static void moveToelement(String ele, String elem)
            	{
            		try {
            		Actions action = new Actions(driver);
            		WebElement we = ResuablesActions.getelement(ele);
            		action.moveToElement(we).build().perform();
            		System.out.println(elem);
            		/*WebDriverWait wait = new WebDriverWait(driver,30);
            	    wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(elem)));*/
            		driver.findElement(By.linkText(elem)).click();
            		//action.moveToElement(target).click().build().perform();
            		
            		
            		
            		}
            		catch(Exception e)
            		{
            			test.log(LogStatus.FAIL, "Exception while moving to the element" );
            			e.printStackTrace();
            		}
            		}
            	public static String GetAttribute(String Webeleloc,String attribute)
				{   
            		String value="NULL";
            		try {
            		
            		
					WebElement ele=ResuablesActions.getelement(Webeleloc);
				   
					 value=ele.getAttribute(attribute);
            		}
            		catch(Exception e)
            		{
            			test.log(LogStatus.FAIL, "Error while getting the attribute for the element "+Webeleloc);
            			e.printStackTrace();
            		}
					return value;
				}
                public static WebDriver ClickandSwitchToWindow(WebDriver driver, String ele)
                { try {

                	ResuablesActions.SleepTime(2000);
                	ResuablesActions.getelement(ele).click();
                	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
               	  

               	 ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles()); 
               	 System.out.println(availableWindows);
               	 
               	 if (!availableWindows.isEmpty()) { 

               		 driver.close();

               	 driver.switchTo().window(availableWindows.get(1)); 
               	 
               	 System.out.println(driver.getTitle());
               	 
               	 }
                }
                catch(Exception e)
                {
                	test.log(LogStatus.FAIL, "Error while switching to new window" );
                     e.printStackTrace();
                }
                
               	 return driver;
                }
                public static boolean assertThat(String s1, String s2)
                {
                	boolean matching=false;
                	if(s1.startsWith(s2, 0))
                	{
                		test.log(LogStatus.PASS, "Same Item has opened in new window");
                		 matching=true;
                	}
                		
                	else { 
                		test.log(LogStatus.FAIL, "Wrong item has opened in new window ");
                		 
                	}
                	
                	System.out.println(matching);
                	return matching;
                	
                }
                public static void JavaScriptClickOnElement(String elementToLocate)
            	{
            		/*This method is to Click on Element, Pass the WebElement as a parameter Value*/
            		try
            		{
            			WebElement Element = driver.findElement(By.xpath(prop.getProperty(elementToLocate)));
            			
            			JavascriptExecutor executor = (JavascriptExecutor)driver;
            		
            			executor.executeScript("var elem=arguments[0]; setTimeout(function() {elem.click();}, 300)", Element);		
            		}catch(org.openqa.selenium.TimeoutException ex)
            		{
            			logger.info("Element Not Found Exception " + ex.getMessage());
            			
            			test.log(LogStatus.FAIL, "Element Not Found Exception " + ex.getMessage());
            			
            			throw(ex);
            		}
            	}
                public static void selectsize(String ele)
                {
       		 
    		 
                	JavascriptExecutor jse = (JavascriptExecutor)driver;
                	List<WebElement> li=driver.findElements(By.xpath(prop.getProperty("sizeselect")));
                	int numberOfItems = li.size();
                	
                	JavascriptExecutor executor = (JavascriptExecutor)driver;
                	jse.executeScript("scroll(0, 400);");
                	for(int i=1;i<=numberOfItems;i++) {
                		if(i>1)
                		{
                			ResuablesActions.JavaScriptClickOnElement("clicksizetable");
                			jse.executeScript("scroll(0, 400);");
                		}
                		WebElement Element = driver.findElement(By.xpath("(.//ul[@id='configurable_swatch_size']//li)"+"["+i+"]//a"));
                		executor.executeScript("var elem=arguments[0]; setTimeout(function() {elem.click();}, 300)", Element);
                		ResuablesActions.SleepTime(2000);
                	try {
                	 driver.findElement(By.xpath(prop.getProperty("outofstock")));
                	 System.out.println(driver.findElement(By.xpath(prop.getProperty("outofstock"))).isDisplayed());
                	 test.log(LogStatus.INFO, "item not available for the size :"+Element.getAttribute("name"));
                	 
                		 
                	}
                	catch(Exception ne)
                	{
                		WebElement we=driver.findElement(By.xpath(prop.getProperty("addtocart")));
                		test.log(LogStatus.INFO, "Item available in the Size: "+Element.getAttribute("name"));
                		ResuablesActions.SleepTime(500);
                		we.click();
                		
                		break;
                	}
                	
                	if(i>numberOfItems)
                	{
                		System.out.println("Product is out of stock");
                	}
                	 
                	}
                	
          		  
                }
}
                 



