package ListernersDemo;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;



public class TestngListerner implements ITestListener
{
	public void onTestFailure(ITestResult result)
	{	
	System.out.println("Test Case Failed");
	}

	public void onTestSkipped(ITestResult result)
	{	
		//logger.info("Test Case Skipped due to unexpected Issue " + result.getName());
	}

	public void onTestStart(ITestResult result)
	{	
		//logger.info("Test Case Started executing " + result.getName());
		
	}

	public void onTestSuccess(ITestResult result)
	{	
		//logger.info("Test case passed successfully " + result.getName());
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result)
	{
	
	}

	public void onStart(ITestContext result)
	{
		
	}

	public void onFinish(ITestContext result)
	{
		//logger.info("Test Execution Finished for Customs Codes Bulk Update");
	}
}
